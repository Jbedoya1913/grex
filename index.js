$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel'). ({
    			intervar: 1000
    		});

    		$('#contacto').on('show.bs.modal', function (e){
    			console.log('el modal contacto se esta mostrando');

    			$('#contactoBtn').removeClass('btn-outline-success');
    			$('#contactoBtn').addClass('btn-primary');
    			$('#contactoBtn').prop('disable',true);
    		});

    		$('#contacto').on('shown.bs.modal', function (e){
    			console.log('el modal contacto se mostro');
    		});

    		$('#contacto').on('hide.bs.modal', function (e){
    			console.log('el modal se oculta');
    		});

    		$('#contacto').on('hidden.bs.modal', function (e){
    			console.log('el modal se oculto');

    			$('#contactoBtn').removeClass('btn-primary');
    			$('#contactoBtn').addClass('btn-outline-success')
    			$('#contactoBtn').prop('disable',false);
    		});

			});